/**
 * Combine all reducers in this file and export the combined reducers.
 */

import {combineReducers} from '@reduxjs/toolkit';
import storage from "redux-persist/lib/storage";
import {persistReducer} from "redux-persist";
import {reducer as loginReducer} from '../components/Auth/features/loginSlice';

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
export default function createReducer(injectedReducers = {}) {
    const persistConfig = {
        key: 'root',
        storage
    }

    const reducers = combineReducers({
        login: loginReducer,
        ...injectedReducers,
    });

    return persistReducer(persistConfig, reducers)
}