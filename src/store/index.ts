import configureStore from "./configureStore";

const initialState = {};
const store = configureStore(initialState);

// Infer the `RootState` and `AppDispatch` types from the index itself
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;