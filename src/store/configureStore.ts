/**
 * Create the store with dynamic reducers
 */
import {configureStore, getDefaultMiddleware} from '@reduxjs/toolkit';
import {createInjectorsEnhancer, forceReducerReload} from 'redux-injectors';
import createSagaMiddleware from 'redux-saga';
import createReducer from './reducers';
import logger from "redux-logger";

export default function configureAppStore(initialState = {}) {
    const reduxSagaMonitorOptions = {};
    const sagaMiddleware = createSagaMiddleware(reduxSagaMonitorOptions);
    const {run: runSaga} = sagaMiddleware;
    const middlewares = [sagaMiddleware, logger];
    const enhancers = [
        createInjectorsEnhancer({
            createReducer,
            runSaga,
        }),
    ];

    const store = configureStore({
        reducer: createReducer(),
        middleware: [...getDefaultMiddleware({
            serializableCheck: {
                // Ignore these action types
                ignoredActions: ['persist/PERSIST']
            }
        }), ...middlewares],
        preloadedState: initialState,
        devTools: true,//process.env.NODE_ENV !== 'production',
        enhancers,
    });

    // Make reducers hot reloadable, see http://mxs.is/googmo
    // @ts-ignore
    if (module.hot) {
        // @ts-ignore
        module.hot.accept('./reducers', () => {
            forceReducerReload(store);
        });
    }

    return store;
}