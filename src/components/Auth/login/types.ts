export type LoginFormType = {
    username: string;
    password: string;
}

export interface ILoginState {
    data: [];
    loading: boolean;
    error: boolean;
}