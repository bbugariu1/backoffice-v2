import React from "react";
import {LoginFormType} from "./types";
import Logo from "../../ui/Logo/logo";
import {Link} from "react-router-dom";
import {useAppDispatch, useAppSelector} from "../../../store/hooks";
import {name, actions} from "../features/loginSlice";
import {useInjectSaga} from "redux-injectors";
import saga from './saga';
import ctx from 'classnames';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSpinner} from "@fortawesome/free-solid-svg-icons/faSpinner";
import SpinnerButton from "../../ui/Spinners/SpinnerButton/SpinnerButton";

const Login = () => {
    useInjectSaga({key: name, saga});
    const {loading, error, data} = useAppSelector(state => state.login);
    const dispatch = useAppDispatch();
    const [values, setValues] = React.useState<LoginFormType>({username: '', password: ''});
    const errorClass = ctx('invisible', {
        // visible: values.username.trim().length === 0
    })

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        dispatch(actions.login(values));
    }

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const {name, value} = event.target;

        setValues({...values, [name]: value});
    }


    return (
        <div className='relative min-h-screen min-w-full flex flex-col items-center bg-gray-100'>
            <form onSubmit={handleSubmit}
                  className='relative w-100 border bg-white rounded-md shadow-lg p-4 mt-20 w-full sm:mt-40 sm:w-96'>
                <Logo/>
                <div className='form-control mt-8'>
                    <input type="text" name='username' placeholder='username' onChange={handleChange}/>
                    <small className={errorClass}>
                    </small>
                </div>
                <div className="form-control">
                    <input type="password" name='password' placeholder='password' onChange={handleChange}/>
                    <small className={errorClass}>
                    </small>
                </div>

                {loading
                    ? <SpinnerButton />
                    : <button type="submit" className='btn secondary w-full'>Submit</button>
                }




                <p className="text-xs text-secondary-light text-center mt-2">
                    Click {" "}
                    <Link to="/reset-password" className="text-info">here</Link> {" "}
                    to recover your password !
                </p>
            </form>
        </div>
    )
}

export default Login;