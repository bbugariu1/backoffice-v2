import request from "../../../utils/requests";
import {LoginFormType} from "./types";

export function login(params: LoginFormType) {
    const requestURL = 'user/login';
    const data = JSON.stringify({username: params.username, password: params.password});
    const options: RequestInit = {
        method: 'POST',
        body: data
    }

    return request(requestURL, options);
}