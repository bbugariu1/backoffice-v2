import {takeEvery, call, put} from 'redux-saga/effects';
import {actions} from '../features/loginSlice';
import * as api from './api';
import {LoginFormType} from "./types";

export function* login({payload}: {payload: LoginFormType}) {
    try {
        const data = yield call(api.login, payload);
        yield put(actions.success({data}));
    } catch (error) {
        yield put(actions.error({error:  error.message}))
    }
}

// Individual exports for testing
export default function* loginSaga() {
    yield takeEvery(actions.login, login);
}