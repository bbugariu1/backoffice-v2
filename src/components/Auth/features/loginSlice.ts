import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {RootState} from "../../../store";
import {ILoginState, LoginFormType} from "../login/types";

// Define the initial state using that type
const initialState: ILoginState = {
    data: [],
    loading: false,
    error: false,
}

export const loginSlice = createSlice({
    name: "login",
    initialState,
    reducers: {
        login: (state, action: PayloadAction<LoginFormType>) => {
            state.loading = true;
            state.error = false;
            state.data = [];
        },
        success: (state, action) => {
            state.data = action.payload.data;
            state.loading = false;
        },
        error: (state, action) => {
            state.error = action.payload.error;
            state.loading = false;
        }
    }
});

// export const {getLogin} = loginSlice.actions;
//
// export const selectData = (state: RootState) => state.login.data;


export const {name, actions, reducer} = loginSlice;