export type ResetEmailFormType = {
    email_1: string;
    email_2: string;
}

export type ResetUsernameFormType = {
    username: string;
}

export enum Forms {
    EMAIL = 'EMAIL',
    USERNAME = 'USERNAME'
}