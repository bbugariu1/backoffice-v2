import React from "react";
import './style.css';
import {Forms, ResetEmailFormType, ResetUsernameFormType} from "./types";
import Logo from "../../ui/Logo/logo";


const ResetPassword = () => {
    const [emails, setEmails] = React.useState<ResetEmailFormType>({email_1: "", email_2: ""});
    const [username, setUsername] = React.useState<ResetUsernameFormType>({username: ""});

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        const {name, value} = event.target;

        if (name === 'username') {
            setUsername({username: value});
        }

        if (["email_1", "email_2"].includes(name)) {
            setEmails({...emails, [name]: value});
        }
    }

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>, form: Forms): void => {
        event.preventDefault();

        if (form === Forms.EMAIL) {
            // @TODO: trigger reset by emails request
            console.log(emails);
        }

        if (form === Forms.USERNAME) {
            // @TODO: trigger reset by username request
            console.log(username);
        }

    }

    return (
        <div className='container border min-h-screen min-w-full bg-gray-100'>
            <div id='reset-password'>
                <Logo/>
                <div className="cards mt-4">
                    <div className='card'>
                        <div className='card-header'>
                            Password reset form by email
                        </div>
                        <div className='card-body'>
                            <form onSubmit={(e) => handleSubmit(e, Forms.EMAIL)}>
                                <div className="form-control">
                                    <input type="text" name="email_1" placeholder="E-mail" onChange={handleChange}/>
                                    <small/>
                                </div>

                                <div className="form-control">
                                    <input type="text" name="email_2" placeholder="Confirm E-mail"
                                           onChange={handleChange}/>
                                    <small/>
                                </div>

                                <button name="reset-by-email" className="btn secondary md w-full">Reset</button>
                            </form>
                        </div>

                    </div>

                    <div className='card'>
                        <div className='card-header'>
                            Password reset form by username
                        </div>
                        <div className='card-body'>
                            <form className="flex flex-col w-full" onSubmit={(e) => handleSubmit(e, Forms.USERNAME)}>
                                <div className="form-control">
                                    <input type="text" name="username" placeholder="Username" onChange={handleChange}/>
                                    <small/>
                                </div>
                                <div className="form-control invisible">
                                    <input type="text"/>
                                    <small/>
                                </div>

                                <button className="btn secondary md w-full">Reset</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    );
}

export default ResetPassword;