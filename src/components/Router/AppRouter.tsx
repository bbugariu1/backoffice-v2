import React from 'react';
import {BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";
import Login from "../Auth/login/Login";
import DefaultLayout from "../ui/layouts/default/DefaultLayout";
import Home from "../Home/Home";
import ResetPassword from "../Auth/reset-password/ResetPassword";
import NoMatch from "./404/NoMatch";
import PlayerAccounts from "../../modules/playerAccounts/Loadable";

const routes = [
    {
        path: "/login",
        component: Login
    },
    {
        path: "/reset-password",
        component: ResetPassword
    },

    // TODO: dynamically extends default routes
    {
        path: "/player-accounts",
        component: PlayerAccounts
    }
];

const AppRouter = () => {
    return (
        <Router>
            <Switch>
                <Route exact path="/login" component={Login}/>
                <Route exact path="/reset-password" component={ResetPassword}/>

                {/* TODO: Create a DefaultLayout route */}

                <Route exact path="/player-accounts">
                    <DefaultLayout>
                        <PlayerAccounts/>
                    </DefaultLayout>
                </Route>

                <Route exact path="/">
                    <DefaultLayout>
                        <Home/>
                    </DefaultLayout>
                </Route>

                <Route path="*" component={NoMatch}/>
            </Switch>
        </Router>
    );
}

export default AppRouter;