import React from "react";
import {LayoutType} from "../types";
import './layout.css';
import Header from "../../../Header/Header";
import Footer from "../../../Footer/Footer";
import AppDrawer from "../../Navigation/AppDrawer";
import {Link} from "react-router-dom";

const DefaultLayout = ({children}: LayoutType) => {

    return (
        <div id='default-layout'>
            <AppDrawer>
                <div className='wrapper'>
                <Header/>

                <main className='main my-4 px-4'>
                    <Link to="/player-accounts">player accounts</Link>
                    {children}
                </main>

                <Footer/>
                </div>
            </AppDrawer>
        </div>
    );
}

export default DefaultLayout;