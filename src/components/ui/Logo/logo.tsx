import React from "react";
import logo from "../../../styles/assets/logo.png";
import './logo.css';

/**
 * @TODO: add a LogoType and pass the asset/image or use <i>/static/media/logo.42c1c128.png</>
 * @constructor
 */
const Logo = () => {
    return (
        <div className='logo'>
            <img src={logo} alt="logo"/>
        </div>
    )
}

export default Logo;