import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSpinner} from "@fortawesome/free-solid-svg-icons/faSpinner";
import React from "react";
import {ISpinnerButton} from "./types";

const SpinnerButton = ({text}: ISpinnerButton) => {

    return (
        <button className="inline-flex btn secondary w-full justify-center items-center cursor-not-allowed" disabled>
            <svg className="animate-spin -ml-1 mr-3 h-5 w-5 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                <FontAwesomeIcon icon={faSpinner}/>
            </svg>
            {text ? text : 'Loading ...'}
        </button>
    );
}

export default SpinnerButton