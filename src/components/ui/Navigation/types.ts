export interface INavigation {
    width?: number;
    children?: React.ReactNode;
}