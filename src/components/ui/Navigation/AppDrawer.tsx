import React from "react";
import {INavigation} from "./types";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faHandPointRight} from "@fortawesome/free-solid-svg-icons/faHandPointRight";
import ctx from "classnames";
import "./styles.css"

const AppDrawer = ({children}: INavigation) => {
    const [isOpen, setIsOpen] = React.useState<boolean>(false);
    const drawerClasses = ctx('sidebar border shadow-lg',{
        'open': isOpen,
        'closed': !isOpen
    })

    return (
        <div id="drawer" className='flex'>
            <div className={drawerClasses}>
                <button className='icon-open' onClick={() => setIsOpen(!isOpen)}>
                    <FontAwesomeIcon icon={faHandPointRight}/>
                </button>
            </div>
            <div className='app-content'>
                {children}
            </div>
        </div>
    );
}

export default AppDrawer;