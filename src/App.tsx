import React from "react";
import './styles/index.css';
import {Provider} from "react-redux";
import {PersistGate} from 'redux-persist/integration/react'
import {persistStore} from 'redux-persist'
import AppRouter from "./components/Router/AppRouter";
import store from "./store";

/**
 * @TODO: Add redux store context provider
 * @constructor
 */
function App() {

    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistStore(store)}>
                <AppRouter/>
            </PersistGate>
        </Provider>
    );
}

export default App;
