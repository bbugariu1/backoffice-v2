import {createSlice} from "@reduxjs/toolkit";

export interface IPlayerAccountsState {
    playerAccounts: IPlayerAccounts
}

export interface IPlayerAccounts {
    name: string;
}

export const initialState: IPlayerAccounts = {
    name: 'player accounts state'
}

const playerAccountsSlice = createSlice({
    name: 'playerAccounts',
    initialState,
    reducers: {
        test: state => {
            console.log(state.name);
        }
    }
});

export const {test} = playerAccountsSlice.actions
export const {name, actions, reducer} = playerAccountsSlice;