import { createSelector } from '@reduxjs/toolkit';
import {name, initialState} from "./slice";

const selectHackerNewsArticlesDomain = (state: any) => state[name] || initialState;

const makeSelectHackerNewsArticles = () =>
    createSelector(selectHackerNewsArticlesDomain, (substate) => substate);

export default makeSelectHackerNewsArticles;
export { selectHackerNewsArticlesDomain };