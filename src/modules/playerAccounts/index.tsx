import { shallowEqual, useSelector, useDispatch } from 'react-redux';
import { useInjectReducer, useInjectSaga } from 'redux-injectors';
import makeSelectHackerNewsArticles from './selectors';
import { name, reducer, actions } from './slice';
import React from "react";

const usePlayerAccounts = ({}) => {
    useInjectReducer({ key: name, reducer });
    const dispatch = useDispatch();
    const store = useSelector(makeSelectHackerNewsArticles(), shallowEqual);

    React.useEffect(() => {
        console.log('loaded')
    }, []);

    return store;
}

export function PlayerAccounts({children, ...props} : any) {
    const store = usePlayerAccounts(props)
    return <>{store.name}</>
}

export default React.memo(PlayerAccounts);