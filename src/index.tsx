import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const MOUNT_NODE = document.getElementById('root');

toast.configure({
    autoClose: 5000,
    draggable: false,
    position: toast.POSITION.TOP_RIGHT,
    containerId: process.env.REACT_APP_UNIQ_ID
});

const Main = (
    <>
        <App/>
    </>
);

ReactDOM.render(Main, MOUNT_NODE);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
