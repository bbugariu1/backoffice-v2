import appConfig from '../../settings.json';
import {toast, ToastContent} from "react-toastify";
export const BASE_USER = appConfig.api_url && appConfig.api_ver ? appConfig.api_url + "/" + appConfig.api_ver + "/" : window.origin + "/api/v1/";

/**
 * @todo: Get rid of the build-id
 */
const defaultOptions: RequestInit = {
    headers: {
        'Content-Type': 'application/json',
        'build-id': 'xWlBHfsrqAiz7',
    }
}

/**
 * @todo define response type/s
 *
 * @param response
 */
function parseJSON(response: Response): null | Promise<any> {
    if (response.status === 204 || response.status === 205) {
        return null;
    }

    return response.json();
}

function checkStatus(response: Response): Response {
    if (response.status >= 200 && response.status < 300) {
        return response
    }

    const error: any = new Error(response.statusText);
    error.response = response;

    toast.error(`${error.response.status}: ${error.message}`);
    throw error;
}

/**
 * Requests a URL, returning a promise
 *
 * @param url
 * @param options
 */
export default async function request(url: RequestInfo, options?: RequestInit) {
    const mergedOptions: RequestInit = {
        ...defaultOptions,
        ...options,
        headers: {
            ...defaultOptions.headers,
            ...options?.headers
        }
    }
    const fetchResponse = await fetch(BASE_USER + url, mergedOptions);
    const response = await checkStatus(fetchResponse);
    return parseJSON(response);
}