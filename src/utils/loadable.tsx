import React, {lazy, ReactElement, Suspense} from 'react';

const loadable = (importFunc: any, { fallback = null } = { fallback: null }) => {
    const LazyComponent = lazy(importFunc);

    return (props: ReactElement | any) => (
        <Suspense fallback={fallback}>
            <LazyComponent {...props} />
        </Suspense>
    );
}

export default loadable;