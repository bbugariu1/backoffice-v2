const colors = require('tailwindcss/colors')
module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      white: colors.white,
      black: colors.black,
      gray: colors.gray,
      orange: {
        light: '#FF8A33',
        DEFAULT: '#FF6D00',
        dark: '#D25A00',
      },
      primary: {
        min: colors.blue["50"],
        lighter: colors.blue["500"],
        light: colors.blue["600"],
        DEFAULT: colors.blue["700"],
        dark: colors.blue["800"],
        darker: colors.blue["900"]
      },
      secondary: {
        min: colors.gray["50"],
        lighter: colors.gray["300"],
        light: colors.gray["400"],
        DEFAULT: colors.gray["500"],
        dark: colors.gray["600"],
        darker: colors.gray["900"]
      },
      success: {
        min: colors.green["50"],
        lighter: colors.green["500"],
        light: colors.green["600"],
        DEFAULT: colors.green["700"],
        dark: colors.green["800"],
        darker: colors.green["900"]
      },
      danger: {
        min: colors.red["50"],
        lighter: colors.red["400"],
        light: colors.red["500"],
        DEFAULT: colors.red["600"],
        dark: colors.red["700"],
        darker: colors.red["800"]
      },
      warning: {
        min: colors.amber["50"],
        lighter: colors.amber["300"],
        light: colors.amber["400"],
        DEFAULT: colors.amber["500"],
        dark: colors.amber["600"],
        darker: colors.amber["700"]
      },
      info: {
        min: colors.purple["50"],
        lighter: colors.purple["500"],
        light: colors.purple["600"],
        DEFAULT: colors.purple["700"],
        dark: colors.purple["800"],
        darker: colors.purple["900"]
      },
    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
